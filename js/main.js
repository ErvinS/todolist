// prva tri reda smo koristili za pozivanje funkcija iz date.js, kada smo imali dropdown za izbor datuma
// createDayDropdown (); // Pozivanje funkcije iz date.js koji daje dane
// createMonthDropdown ();
// createYearDropdown ();

const now = new Date();


$(document).ready(function() {

// ubacujemo kalendar
    $('#deadline').datepicker({
        dateFormat: "yy-mm-dd",
        minDate: '+1d'
      });

// aktivacija filtera
 $('#actionFilter').change(function(){

        let filter = {}
        if($(this).val() !== '') {
            filter = {
                action: $(this).val()
            }
        }

// Brisanje podataka
        const ul = document.querySelector('.cont_princ_lists > ul');
        ul.innerHTML = '';

// Pozivanje vec sacuvanih podataka koji se filterisu
        $.ajax({
            url: 'https://my-json-server.typicode.com/nebojsazr/todo_service/todos',
            method: 'GET',
            data: filter,
            dataType: 'json'
        }).done(function(data) {
            if (data.length > 0) {
                for(i=0; i<data.length; i++) {
                    data[i].date = new Date( data[i].date );
                    createListItem( data[i] );
                }
            }
        }).fail();
    });

    
    let test;
    /// Pozivanje vec sacuvanih podataka preko ajaxa
    $.ajax({
        url: 'https://my-json-server.typicode.com/nebojsazr/todo_service/todos',
        method: 'GET',
        dataType: 'json'
    }).done(function(data) {
        if (data.length > 0) {
            for(i=0; i<data.length; i++) {
                data[i].date = new Date( data[i].date );
                createListItem( data[i] );
            }
        }

        test = 'Ovo je neka vrednost'

    }).fail();

    setTimeout(function(){
        console.log('TEST: ' + test);
    },1000);
    
});



// funkcija za otvaranje i sklanjanje novih formi
function add_new() {
	// selektovati element  u kojem se nalazi forma
	const forma = document.querySelector('.cont_crear_new');

	// Dodavanje ili oduzimanje css klase koja cini da forma postane vidljiva
	forma.classList.toggle('cont_crear_new_active');
}

// funkcija za dodadavanje novih stavki na listu
function add_to_list() {


		/*	// ubacujemo vrednosti datuma
			const day = document.getElementById('dateContainer').children[0].value;
			const month = document.getElementById('monthContainer').children[0].value;
			const year = document.getElementById('yearContainer').children[0].value;

			const deadLine = new Date (year, month, day); 

			monthNames = [
        		'Jan','Feb','Mar','Apr','May','Jun','Jul','Avg','Sep','Okt','Nov','Dec'
    		];
		*/

		const deadLine = new Date(document.getElementById('deadline').value);


// selektovanje vrednosti - pravimo objekat koji ima ime elements
	const elements = {
		action: document.getElementById('action_select'),
		title: document.querySelector('.input_title_desc'),
		date: deadLine, // ima nacin da se kroz deadLine.toDateString() doda u html
		description: document.querySelector('.input_description')
		}

	// pokrecemo funkciju koja proverava da li su polja naslov i opis popunjena
	// funkcija se nalazi dole
	if (!isValid(elements)) {
		return false;
	}  

	createListItem({                          //funkcija koja ce kreirati elemente na osnovu onog so upisemo
		action: elements.action.value,			// prebacujemo upisane i odredjene podatke
		title: elements.title.value,
		description: elements.description.value,
		date: elements.date
	});

}

function createListItem( itemData ) {


// kreiramo ul u koji ce ici il, i il u koji ce ici divovi (elemente i cvorove)
  	const ul = document.querySelector('.cont_princ_lists > ul');
	const li = document.createElement('li');
	li.classList.add('list_shopping');

		//pravimo klasu li elemntu,koji ce biti drugaciji za svaki novi unos u listu
		let childNum = ul.children.length;
		const itemClass = 'li_num_0_' + (Number(childNum) + 1);
		li.classList.add( itemClass );

// kreiramo div za akciju
	const div_action = document.createElement('div');
	div_action.className = 'col_md_1_list';
	div_action.innerHTML = '<p>' + itemData.action + '</p>';
// kreiramo div za naslov i opis
	const div_titdesc = document.createElement('div');
	div_titdesc.className = 'col_md_2_list';
	// naslov
	const div_title = document.createElement('h4');
	div_title.textContent = itemData.title;
	// opis
	const div_description = document.createElement('p');
	div_description.classList.add('desc' + (Number(childNum) + 1)); // dodajemo jedinstvenu klasu svakoj opisu
	div_description.textContent = itemData.description;

		// da se opis pojavi i nestane kada se klikne na naslov
		div_title.addEventListener('click', function() {
			let desc = document.getElementsByClassName('desc' + (Number(childNum) + 1))[0];
			desc.classList.toggle('hidden');
		});

	// dodajemo div sa naslovom i opisom u zajednicki div
	div_titdesc.appendChild(div_title);
	div_titdesc.appendChild(div_description);
// kreiramo div za vreme
	const div_date = document.createElement('div'); 
	div_date.className = 'col_md_3_list';

	const div_date_text = document.createElement('div'); 
	div_date_text.className = 'cont_text_date';

	const date_text = document.createElement('p');
	date_text.textContent = itemData.date.toLocaleDateString(); // ovde ubacujemo datum

	div_date_text.appendChild(date_text);
	div_date.appendChild(div_date_text);

// dodavanje elemenata u html
	li.appendChild(div_action);
	li.appendChild(div_titdesc);
	li.appendChild(div_date);

	ul.appendChild(li);


			//dodavanje dugmeta i funkcije da obrisemo element
			const deleteBtn = document.createElement('button'); //pravimo dugmo
			deleteBtn.innerHTML = 'x'; 							// oblik dugmet  u html
			deleteBtn.className = 'button_elem';				// dodajemo klasu dugmetu
			deleteBtn.addEventListener('click', function () {

				if(confirm('Are you sure you want to delete this task?')) {
				ul.removeChild(li);
				}
				
			});	// dodajemo event da na klik pita da li zelimo brisati, i ako da, da pokrene funkciju koja brise item

			date_text.appendChild(deleteBtn); // dodajemo dugme u html



	// Kreiram futer
	const itemFooter = document.createElement('div');
	itemFooter.classList.add('item_footer');
	li.appendChild(itemFooter);

	// kreiranje tajmera
	const leftTimeContainer = document.createElement('span');
	leftTimeContainer.classList.add('left_time_container');
	let leftTimeContainerText = 'Time left: ';
	let leftTime = leftUntil ( itemData.date );
		if (now.setHours(0,0,0,0) == itemData.date.setHours(0,0,0,0) ) { // proverava da li je uneti datum danas
				leftTimeContainer.classList.add('left_time_container_red');
				leftTimeContainerText += 'TODAY is the LAST DAY';
			}
			else {

				leftTimeContainerText += leftTime.years + ' years, ' + leftTime.months + ' months, ' + leftTime.days + ' days, ' + leftTime.hours + ' hours, ' + leftTime.minutes + ' minutes, ' + leftTime.seconds + ' seconds';
	}

	leftTimeContainer.textContent = leftTimeContainerText;
	itemFooter.appendChild(leftTimeContainer);



// forma se ocisti, refresuje posle dodavanja
	document.getElementById('action_select').value = 'SHOPPING';
	document.querySelector('.input_title_desc').value = '';
	//document.getElementById('date_select').value = 'TODAY'; // ubacili datume pa ovo vise ne treba
	document.querySelector('.input_description').value = '';

	//document.getElementById('dateContainer').children[0].value = new Date().getUTCDate();
	//document.getElementById('monthContainer').children[0].value = new Date().getUTCMonth();
	//document.getElementById('yearContainer').children[0].value = new Date().getUTCFullYear();

}


// funkcija koja proverava da li su polja naslov i opis popunjena
function isValid(elem) {

			// ako ponovo udjemo u validaciju, da pocistimo stanje invalid
			elem.title.classList.remove('invalid');
			elem.description.classList.remove('invalid');

	let result = true;

	// if (elem.title.value == '') { ---ovo samo proverava da li je nesto upisano ili ne
	if ( String(elem.title.value).replace(' ', '') == '') {  //a ovako prazno mesto(space),isto dozivljava kao da ne pise nista

			// Uokviriti input za naslov ako je los upis
			elem.title.classList.add('invalid');

		result = false;
	}

	// if (elem.description.value == '') {---isto, ovo samo proverava da li je nesto upisano ili ne
	if ( String(elem.description.value).trim() == '') {   // isto, space ne prepoznaje kao nesto, vec kao nista

			// Uokviriti input za opis ako je los upis
			elem.description.classList.add('invalid');

		result = false;
	}


			// da proverimo da li je unet datum u buducnosti
			

			if (now.setHours(0,0,0,0) == elem.date.setHours(0,0,0,0) ) { // proverava da li je uneti datum danas
				

				if ( String(elem.title.value).replace(' ', '') == '') { // naredni redovi proveravaju za danasnji dan da li su uneti podaci
					elem.title.classList.add('invalid');
					result = false; }
				else if ( String(elem.description.value).trim() == '') {
					elem.description.classList.add('invalid');
					result = false;
				}

				else result = true;
			}
			else if (now.getTime() > elem.date.getTime()) {

				alert ('This is a list for your future plans, not for your regrets from past!!!');

				result = false;
			}


	
	return result
}
